/*  (c) 2021 matthew fairchild
 *  
 *  helper class that wraps the new XR functionality for easier use
 */

using System.Collections.Generic;
using UnityEngine;
using UnityEngine.XR;

namespace EightBitDinosaur
{
	public static class DinoXR
	{
	    // the device used in XRNode calls. member variable to avoid frequent GC
	    private static List<InputDevice> m_devices = new List<InputDevice>();
	
		/// <summary>
		/// retrieve the device rotation in localspace of a device with the given characteristics
		/// </summary>
		/// <param name="n_characteristics">characteristics of the device to retrieve the orientation from</param>
		/// <param name="n_rot">out value that will be filled with the rotation</param>
		/// <returns>whether a device with the given characteristics was found</returns>
	    public static bool get_device_rotation(InputDeviceCharacteristics n_characteristics, out Quaternion n_rot)
	    {
	        InputDevices.GetDevicesWithCharacteristics(n_characteristics, m_devices);
	
	        if (m_devices.Count == 0)
	        {
	            Debug.LogWarning($"No devices found with the given characteristics; {n_characteristics}.");
	            n_rot = Quaternion.identity;
	            return false;
	        }
	
	        if (m_devices.Count > 1)
	        {
	            Debug.LogWarning($"More than One Device found with characteristics; {n_characteristics}.\nProceeding with first one found");
	        }
	
	        if (m_devices[0].isValid)
	        {
	            if (m_devices[0].TryGetFeatureValue(CommonUsages.deviceRotation, out n_rot))
	            {
	                return true;
	            }
	        }
	
	        // in this case there was no result for the given device
	        n_rot = Quaternion.identity;
	        return false;
	    }

		/// <summary>
		/// retrieve the device position in localspace of a device with the given characteristics
		/// </summary>
		/// <param name="n_characteristics">characteristics of the device to retrieve the orientation from</param>
		/// <param name="n_pos">out variable that will be filled with the position</param>
		/// <returns>whether a device with the given characteristics was found</returns>
		public static bool get_device_position(InputDeviceCharacteristics n_characteristics, out Vector3 n_pos)
	    {
	        InputDevices.GetDevicesWithCharacteristics(n_characteristics, m_devices);
	
	        if (m_devices.Count == 0)
	        {
	            Debug.LogWarning($"No devices found with the given characteristics; {n_characteristics}.");
	            n_pos = Vector3.zero;
	            return false;
	        }
	
	        if (m_devices.Count > 1)
	        {
	            Debug.LogWarning($"More than One Device found with characteristics; {n_characteristics}.\nProceeding with first one found");
	        }
	
	        if (m_devices[0].isValid)
	        {
	            if (m_devices[0].TryGetFeatureValue(CommonUsages.devicePosition, out n_pos))
	            {
	                return true;
	            }
	        }
	
	        // in this case there was no result for the given device
	        n_pos = Vector3.zero;
	        return false;
	    }

		/// <summary>
		/// set the tracking origin of all retrievable XR Input Subsystems
		/// </summary>
		/// <param name="n_mode">the tracking mode to be set to</param>
		/// <returns>true if all subsystems set successfully, false if any one returned false in the process</returns>
		public static bool SetTrackingOrigin(TrackingOriginModeFlags n_mode)
		{
			bool result = true;
            List<XRInputSubsystem> subsystems = new List<XRInputSubsystem>();
            SubsystemManager.GetInstances<XRInputSubsystem>(subsystems);
            for (int i = 0; i < subsystems.Count; i++)
            {
                result = result && subsystems[i].TrySetTrackingOriginMode(n_mode);
                subsystems[i].TryRecenter();
            }
			return result;
        }
    }
}
